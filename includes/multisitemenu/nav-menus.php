<?php

namespace Boldface\MultisiteMenu;

defined( 'ABSPATH' ) or die();

/**
 * Class for interfacing with the navigation menus
 *
 * @package Boldface\MultisiteMenu
 */
class nav_menus {

  /**
   * @var object WP_User object
   *
   * @access private
   * @since 0.1
   */
  private $user;

  /**
   * @var array Array of removed arguments
   *
   * @access private
   * @since 0.1
   */
  private $sites;

  /**
   * @var int Menu ID
   *
   * @access private
   * @since 0.1
   */
  private $menu_id;

  /**
   * @var int Site ID
   *
   * @access public
   * @since 0.1
   */
  public $site_id;

  /**
   * Object constructor
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $user = null ) {
    $this->user = null === $user ? \wp_get_current_user() : $user;

    $this->sites = \get_sites();

    $this->menu_id = isset( $_REQUEST[ 'menu' ] ) ? intval( $_REQUEST[ 'menu' ] ) : null;

    $this->site_id = $this->site_from_menu();
  }

  /**
   * Determine the site that the menu_id belongs to
   *
   * @access private
   * @since 0.1
   *
   * @return int The blog ID that the menu ID belongs
   */
  private function site_from_menu() {
    foreach( $this->sites as $site ) {
      \switch_to_blog( $site->blog_id );
      foreach( $this->menus() as $menu ) {
        if( $menu->term_id === $this->menu_id ) {
          \restore_current_blog();
          return $site->blog_id;
        }
      }
      \restore_current_blog();
    }
  }

  /**
   * Return an array of menus the user is allowed to edit
   *
   * @access public
   * @since 0.1
   *
   * @return array Array of menus
   */
  public function user_menus() {
    $menus = $this->multisite_loop( [ $this, 'menus' ] );
    return $this->cleanup_menus( $menus );
  }

  /**
   * Cleanup the menus
   *
   * @param array Array of dirty menus
   *
   * @access private
   * @since 0.1
   *
   * @return array Array of clean menus
   */
  private function cleanup_menus( $menus ) {
    $_menus = [];
    foreach( $menus as $site_id => $menu ) {
      if( is_array( $menu ) ){
        foreach( $menu as &$_menu ) {
          $_menu = $this->rename_menu( $_menu, $site_id );
        }
        $_menus = array_merge( $_menus, $menu );
      }
    }
    return $_menus;
  }

  /**
   * Get the menus
   *
   * @access public
   * @since 0.1
   *
   * @return array Array of menus
   */
  public function menus() {
    if( ! \user_can( $this->user, 'edit_theme_options' ) ) {
      return [];
    }
    foreach( \wp_get_nav_menus() as $menu ) {
      $menus[] = $menu;
    }
    return $menus;
  }

  /**
   * Wrapper to loop all multi sites
   *
   * @param callable $callback Callback
   * @param array    $args     Arguments
   *
   * @access private
   * @since 0.1
   *
   * @return mixed Whatever the callback returns
   */
  private function multisite_loop( $callback, $args = [] ) {
    foreach( $this->sites as $site ) {
      \switch_to_blog( $site->blog_id );
      $return[ $site->blog_id ] = call_user_func_array( $callback, $args );
      \restore_current_blog();
    }
    return $return;
  }

  /**
   * Rename the menu
   *
   * @param WP_Menu object $menu The menu
   * @param int Site ID
   *
   * @access private
   * @since 0.1
   *
   * @return WP_Menu object The renamed menu
   */
  private function rename_menu( $menu, $site_id ) {
    if( ! is_object( $menu ) || ! isset( $menu->name ) ) {
      return $menu;
    }

    \switch_to_blog( $site_id );
    $menu->name = sprintf(
      'Site: %1$s &mdash; %2$s',
      \get_bloginfo( 'name' ),
      $menu->name
    );
    \restore_current_blog();
    return $menu;
  }
}
