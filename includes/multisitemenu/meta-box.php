<?php

namespace Boldface\MultisiteMenu;

defined( 'ABSPATH' ) or die();

/**
 * Class for rendering the Multi Menu meta box
 *
 * @package Boldface\MultisiteMenu
 */
class meta_box {

  /**
   * @var array Array of sites
   *
   * @access private
   * @since 0.1
   */
  private $sites;

  /**
   * @var object Walker Nav Menu Checklist Object
   *
   * @access private
   * @since 0.1
   */
  private $walker;

  /**
   * @var array Array of removed arguments
   *
   * @access private
   * @since 0.1
   */
  private $removed_args;

  /**
   * Object constructor
   *
   * @access public
   * @since 0.1
   */
  public function __construct() {
    $this->sites = \get_sites();
    $this->walker = new \Walker_Nav_Menu_Checklist();
    $this->removed_args = [
      'action',
      'customlink-tab',
      'edit-menu-item',
      'menu-item',
      'page-tab',
      '_wpnonce'
    ];
  }

  /**
   * Render Multisite Menu Meta Box
   *
   * @access public
   * @since 0.1
   */
  public function render() {
    global $nav_menu_selected_id;

    foreach( $this->sites as &$site ) {
      \switch_to_blog( $site->blog_id );
      $site->classes = array();
    	$site->type = 'custom';
    	$site->object_id = $site->blog_id;
    	$site->title = \get_bloginfo( 'name' );
    	$site->object = 'custom';
    	$site->url =\get_bloginfo( 'url' );
    	$site->attr_title = $site->domain;
      \restore_current_blog();
    }
    $current_tab = 'all';
    ?>
    <div id="site-archive" class="categorydiv">
    	<ul id="site-archive-tabs" class="site-archive-tabs add-menu-item-tabs">
    		<li <?php echo ( 'all' == $current_tab ? ' class="tabs"' : '' ); ?>>
    			<a class="nav-tab-link" data-type="tabs-panel-site-archive-all" href="<?php if ( $nav_menu_selected_id ) echo esc_url( add_query_arg( 'site-archive-tab', 'all', remove_query_arg( $this->removed_args ) ) ); ?>#tabs-panel-site-archive-all">
    				<?php _e( 'View All' ); ?>
    			</a>
    		</li><!-- /.tabs -->
    	</ul>
    	<div id="tabs-panel-site-archive-all" class="tabs-panel tabs-panel-view-all <?php echo ( 'all' == $current_tab ? 'tabs-panel-active' : 'tabs-panel-inactive' ); ?>">
    		<ul id="site-archive-checklist-all" class="categorychecklist form-no-clear">
    		<?php
    			echo \walk_nav_menu_tree( array_map('wp_setup_nav_menu_item', $this->sites ), 0, (object) array( 'walker' => $this->walker) );
    		?>
    		</ul>
    	</div><!-- /.tabs-panel -->
    	<p class="button-controls wp-clearfix">
    		<span class="list-controls">
    			<a href="<?php echo esc_url( add_query_arg( array( 'site-archive-tab' => 'all', 'selectall' => 1, ), remove_query_arg( $this->removed_args ) )); ?>#site-archive" class="select-all"><?php _e('Select All'); ?></a>
    		</span>
    		<span class="add-to-menu">
    			<input type="submit"<?php \wp_nav_menu_disabled_check( $nav_menu_selected_id ); ?> class="button-secondary submit-add-to-menu right" value="<?php esc_attr_e('Add to Menu'); ?>" name="add-site-archive-menu-item" id="submit-site-archive" />
    			<span class="spinner"></span>
    		</span>
    	</p>
    </div><!-- /.categorydiv -->
  <?php
  }
}
