<?php

namespace Boldface\MultisiteMenu;

defined( 'ABSPATH' ) or die();

/**
 * Class for loading the Multisite Menu plugin
 *
 * @package Boldface\MultisiteMenu
 */
class plugin {

  /**
   * @var string Plugin basename
   *
   * @access private
   * @since 0.1
   */
  private $file;

  /**
   * Object constructor
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file = '' ) {
    $this->file = is_string( $file ) ? plugin_basename( $file ) : '';
    $this->nav_menus = new nav_menus();
  }

  /**
   * Method needs to be fired before init.
   * Add several actions to load the plugin.
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    //* Fires on initiation to load plugin text domain
    \add_action( 'init', [ $this, 'load_textdomain' ] );

    if( \is_multisite() && 'POST' !== $_SERVER[ 'REQUEST_METHOD' ] ) {
      \add_action( 'load-nav-menus.php', [ $this, 'nav_menus_loaded' ] );
    }
  }

  /**
   * Maybe add hooks to the admin head on nav-menus.php
   *
   * @access public
   * @since 0.1
   */
  public function nav_menus_loaded() {
    if( \current_user_can( 'manage_network_menus' ) ) {
      \add_action( 'admin_head-nav-menus.php', [ $this, 'add_meta_box' ] );
      \add_filter( 'wp_get_nav_menus', [ $this, 'wp_get_nav_menus' ], 10, 2 );
      \add_filter( 'theme_mod_nav_menu_locations', [ $this, 'theme_mod_nav_menu_locations' ], 10, 1 );
    }
  }

  /**
   * Switch to the correct blog and get the theme mods
   *
   * @access public
   * @since 0.1
   *
   * @return array Array of nav menu locations
   */
  public function theme_mod_nav_menu_locations( $mod ) {
    \switch_to_blog( $this->nav_menus->site_id );
    $mods = \get_theme_mods();
    return $mods[ 'nav_menu_locations' ];
  }

  /**
   * Return all the nav menus that this user can edit no matter which site
   * they are currently editing.
   *
   * @access public
   * @since 0.1
   *
   * @return array All menus the the current user can edit
   */
  public function wp_get_nav_menus( $menu, $args ) {
    \remove_filter( 'wp_get_nav_menus', [ $this, 'wp_get_nav_menus' ], 10, 2 );
    return $this->nav_menus->user_menus();
  }

  /**
   * Add meta box
   *
   * @access public
   * @since 0.1
   */
  public function add_meta_box() {
    \add_meta_box(
      'boldface-multisite-menu',
      'Sites',
      [ new meta_box(), 'render' ],
      'nav-menus',
      'side',
      'high'
    );
  }

  /**
   * Load the text domain
   *
   * @access public
   * @since 0.1
   */
  public function load_textdomain() {
    \load_plugin_textdomain( 'boldface-multisite-menu', false, dirname( \plugin_basename( $this->file ) ) . '/languages' );
  }
}
