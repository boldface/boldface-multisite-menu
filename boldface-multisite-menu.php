<?php
/**
 * Plugin Name: Boldface Multisite Menu
 * Plugin URI: http://www.boldfacedesign.com/plugins/boldface-multisite-menu/
 * Description: Allow users with manage_network_menus to manage the network menus.
 * Version: 0.1
 * Author: Nathan Johnson
 * Author URI: http://www.boldfacedesign.com/author/nathan/
 * Licence: GPL2+
 * Licence URI:
 * Domain Path: /languages
 * Text Domain: boldface-multisite-menu
 */

//* Don't access this file directly
defined( 'ABSPATH' ) or die();

//* Start bootstraping the plugin
require( dirname( __FILE__ ) . '/includes/bootstrap.php' );
add_action( 'plugins_loaded', array( $bootstrap = new boldface_multisite_menu_bootstrap( __FILE__ ), 'register' ) );

//* Register activation and deactivation hooks
register_activation_hook( __FILE__ , array( $bootstrap, 'activation' ) );
register_deactivation_hook( __FILE__ , array( $bootstrap, 'deactivation' ) );
